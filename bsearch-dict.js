var fs = require('fs');

var fileDescriptor = fs.openSync("wordlist.txt", 'r');
var fileLength = fs.fstatSync(fileDescriptor).size;
var wordCache = new Map();

function processBuffer(word, wordBuffer, maxLen) {
	let priorSentinel = -1;
	let charBuf = [];
	let verdict = ["error", "internal error"];
	for (let i = 0; i < maxLen; i++) {
		let c = wordBuffer.readUInt8(i);
		if (c == 10) { /* newline */
			if (priorSentinel != -1) {
				let wordSeq = Buffer.from(charBuf).toString('utf-8');
				let wordSeqParts = wordSeq.split(':', 2);
				if (wordSeqParts.length != 2) return ["error", "unrecognized string " + wordSeq];
				wordCache.set(wordSeqParts[1], wordSeqParts[0]);
				if (word < wordSeqParts[1]) {
					if (verdict[0] == "splitRight") {
						return ["found", undefined];
					}
					return ["splitLeft", priorSentinel];
				} else if (word == wordSeqParts[1]) {
					return ["found", JSON.parse(wordSeqParts[0])];
				} else {
					verdict = ["splitRight", i + 1];
				}
			}
			priorSentinel = i;
			charBuf = [];
		} else if (priorSentinel != -1) {
			charBuf.push(c);
		}
	}
	return verdict;
}

exports.readWord = word => {
	let c = wordCache.get(word);
	if (c) return {result: JSON.parse(c)};
	let minP = 0;
	let maxP = fileLength;
	while (minP < maxP) {
		let tmpBuf = Buffer.alloc(150);
		let initOffset = Math.floor(Math.max(0, (maxP + minP) / 2 - 30));
		let nrRead = fs.readSync(fileDescriptor, tmpBuf, 0, 150, initOffset);
		if (nrRead < 0) {
			return {err: "cannot read from dictionary"};
		}
		if (nrRead == 0) {
			return {err: "zero read"};
		}
		let r = processBuffer(word, tmpBuf, nrRead);
		switch(r[0]) {
			case "splitLeft":
				maxP = initOffset + r[1];
				break;
			case "splitRight":
				minP = initOffset + r[1];
				break;
			case "found":
				return {result: r[1]};
			default:
				return {err: r[1]};
		}
	}
	return {};
}
