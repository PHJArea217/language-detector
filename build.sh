#!/bin/bash

echo > wordlist.txt

javac LDBackend.java && java LDBackend /usr/share/dict/{american-english,ngerman,french,spanish,dutch,italian} \
	<(iconv -f iso8859-1 -t utf-8 /usr/share/dict/portuguese) <(iconv -f iso8859-1 -t utf-8 /usr/share/dict/swedish) \
	>> wordlist.txt
