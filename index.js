var languages = require("./languages.json");
var bsearch_dict = require('./bsearch-dict.js');

exports.language_detector = (req, res) => {
	res.set("Access-Control-Allow-Origin", "*");
	var text = req.query.text;
	var useJson = req.query.format !== "plain";
	if (text === null || text == undefined) {
		res.status(400).send(useJson ? {error: "Text param is required"} : "Error: text param is required");
		return;
	}
	if (!Array.isArray(languages)) {
		res.status(500).send(useJson ? {error: "N/A"} : "Error");
		return;
	}
	var textParts = String(text).split(/[\s'-]/);
	var results = [];
	var accumulator = new Array(languages.length);
	accumulator.fill(0);
	for (let i in textParts) {
		let word = textParts[i].toUpperCase();
		let mapping_r = bsearch_dict.readWord(word);
		if (mapping_r.err) {
			console.log(mapping_r.err);
			res.sendStatus(500);
			return;
		}
		let mapping = mapping_r.result;
		if (Array.isArray(mapping)) {
			mapping.forEach((lang) => accumulator[lang]++);
			results.push({word: word, languages: mapping});
		} else if (mapping != undefined && mapping >= 0) {
			accumulator[mapping]++;
			results.push({word: word, languages: [mapping]});
		} else {
			results.push({word: word, languages: []});
		}
		for (let lv in languages) {
			let charList = languages[lv].c;
			if (!Array.isArray(charList)) continue;
			for (let c of charList) {
				if (word.includes(c)) {
					results.push({word: word, language: lv, specialChar: c});
					accumulator[lv]++;
				}
			}
		}
	}
	var r = new Array(languages.length);
	var max = null;
	for (let i = 0; i < accumulator.length; i++) {
		let langResult = [languages[i].l, accumulator[i]];
		if (!Array.isArray(max) || langResult[1] > max[1]) {
			max = langResult;
		}
		r[i] = langResult;
	}
	var resultJson = {results: results, aggregates: r, winner: max};
	if (req.query.langOnly) {
		res.send(useJson ? {language: max[0]} : max[0]);
		return;
	}
	if (useJson) {
		res.send(resultJson);
		return;
	}
	var resultText = [];
	for (let line of results) {
		if (line.hasOwnProperty('languages')) {
			resultText.push(`--- ${line.word} ---`);
			for (var l of line.languages) {
				if (l >= 0) {
					resultText.push(languages[l].l + ": " + line.word);
				}
			}
		} else if (line.hasOwnProperty('specialChar')) {
			resultText.push(`${languages[line.language].l}: ${line.specialChar} (C)`);
		}
	}
	r.forEach((i) => resultText.push(i[0] + ": " + i[1]));
	resultText.push(max[0]);
	res.set('Content-Type', 'text/plain; charset=utf-8');
	res.send(resultText.join("\n"));
};