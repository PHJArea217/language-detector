import java.util.BitSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;
import java.util.Scanner;
import java.io.FileInputStream;
import java.util.Arrays;
public class LDBackend {
	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.println("Need at least one wordlist as command-line argument");
			System.exit(101);
		}
		Map<String, BitSet> dictionary = new TreeMap<>();
		for (int i = 0; i < args.length; i++) {
			try {
				String path = args[i];
				FileInputStream f = new FileInputStream(path);
				Scanner lineScanner = new Scanner(f);
				BitSet langmask = new BitSet();
				langmask.set(i);
				int[] totalWords = {0, 0};
				while (lineScanner.hasNextLine()) {
					String word = lineScanner.nextLine().toUpperCase();
					if (word.matches(".*\\s.*")) continue;
					dictionary.compute(word, (key, oldWord) -> {
						if (oldWord == null) {
							oldWord = new BitSet();
							totalWords[0]++;
						}
						oldWord.or(langmask);
						totalWords[1]++;
						return oldWord;
					});
				}
				System.err.format("%s: %d words, %d new\n", path, totalWords[1], totalWords[0]);
				lineScanner.close();
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(100);
			}
		}
		/* System.out.println("{"); */
		int numberOfWords = 0;
		for (Map.Entry<String, BitSet> e : dictionary.entrySet()) {
			String k = e.getKey().replaceAll("[\\\"]", "");
			int[] v = e.getValue().stream().toArray();
			if (v.length == 1) {
				System.out.format("%x:%s\n", v[0], k);
			} else {
				System.out.format("%s:%s\n", Arrays.toString(v).replaceAll(" ", ""), k);
			}
			if (numberOfWords++ % 50000 == 0) {
				System.err.format("Processing %d words...\n", numberOfWords);
			}
		}
		/* System.out.println("}"); */
		System.err.printf("%d total words.\n", numberOfWords);
	}
}